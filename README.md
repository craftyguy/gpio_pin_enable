### Description
This is a simple tool for enabling/exporting a list of gpio pins. Pins are
defined in a config file, which is either in the current working directory of
the tool or installed under `/etc/conf.d/gpio_pin_enable.conf`. Each line in
the config file contains the pin number, a 0 or 1 indicating whether or not the
pin should be active_low, and the pin direction (in[put] or out[put]), all
delimited by commas.
See the included `config.d/gpio_pin_enable.conf` for more information.
